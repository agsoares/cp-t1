import os
import rawpy
import imageio
import numpy as np

def colorMask(x, y):
    if (y%2 == 0) and (x%2 == 0):
        return 0
    if (y%2 == 1) and (x%2 == 1):
        return 2
    return 1

out_folder = "./out"

if not os.path.exists(out_folder):
    os.makedirs(out_folder)

raw = rawpy.imread('scene_raw.dng')
rh = raw.sizes.height
rw = raw.sizes.width
ri = raw.raw_image_visible


imageio.imwrite(out_folder + '/scene_raw_1.jpg', ri)

im = np.zeros([rh,rw,3])

for y in range(0, rh):
    for x in range(0, rw):
        im[y][x][colorMask(x,y)] = ri[y][x]

imageio.imwrite(out_folder + '/scene_raw_2.jpg', im)

# http://www.helicontech.co.il/?id=bayer-rgb
for y in range(0, rh):
    for x in range(0, rw):
        cc = colorMask(x,y)
        if cc == 1:
            c1 = 0
            c2 = 0
            i  = 0
            if x > 0:
                c1 += ri[y][x-1]
                i+=1
            if x < rw-1:
                c1 += ri[y][x+1]
                i+=1
            c1 = c1/i
            i = 0
            if y > 0:
                c2 += ri[y-1][x]
                i+=1
            if y < rh-1:
                c2 += ri[y+1][x]
                i+=1
            c2 = c2/i
            if x%2 == 1:
                im[y][x][0] = c1
                im[y][x][2] = c2
            else:
                im[y][x][2] = c1
                im[y][x][0] = c2
        else:
            c1 = 0
            c2 = 0
            i  = 0
            if x > 0:
                c1 += ri[y][x-1]
                i+=1
            if x < rw-1:
                c1 += ri[y][x+1]
                i+=1
            if y > 0:
                c1 += ri[y-1][x]
                i+=1
            if y < rh-1:
                c1 += ri[y+1][x]
                i+=1
            c1 = c1/i
            i=0
            if x > 0    and y > 0:
                c2 += ri[y-1][x-1]
                i+=1
            if x < rw-1 and y > 0:
                c2 += ri[y-1][x+1]
                i+=1
            if x > 0    and y < rh-1:
                c2 += ri[y+1][x-1]
                i+=1
            if x < rw-1 and y < rh-1:
                c2 += ri[y+1][x+1]
                i+=1
            c2 = c2/i
            im[y][x][1] = c1
            if cc == 0:
                im[y][x][2] = c2
            else:
                im[y][x][0] = c2

imageio.imwrite(out_folder + '/scene_raw_3.jpg', im)

#White Balance
r = 0
g = 0
b = 0
for y in range(0, rh):
    for x in range(0, rw):
        r += im[y][x][0]
        g += im[y][x][1]
        b += im[y][x][2]

r_avg = r/(rh*rw)
g_avg = g/(rh*rw)
b_avg = b/(rh*rw)

alpha = g_avg/r_avg
beta  = g_avg/b_avg
for y in range(0, rh):
    for x in range(0, rw):
        im[y][x][0]= alpha *im[y][x][0]
        im[y][x][2]= beta  *im[y][x][2]

imageio.imwrite(out_folder + '/scene_raw_4.jpg', im)

#Gamma Compression
for y in range(0, rh):
    for x in range(0, rw):
        im[y][x] = np.power(im[y][x], 1/2.2)

imageio.imwrite(out_folder + '/scene_raw_5.jpg', im)
